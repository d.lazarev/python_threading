import concurrent.futures
import time


main_counter: int = 0


def worker():
    global main_counter
    
    print("Worker thread is running")
    while main_counter < 100:
        main_counter += 1
        print(f"In thread: main_counter={main_counter}")
        time.sleep(1)
    
    
pool = concurrent.futures.ThreadPoolExecutor(max_workers=2)

pool.submit(worker)
pool.submit(worker)

pool.shutdown(wait=True)

print(f"Main thread continuing to run. main_counter={main_counter}")