import logging
import threading
import time


def worker(name):
    logging.info(f'Thread {name} starting')
    time.sleep(2)
    logging.info(f'Thread {name} finished')
    
    
if __name__ == "__main__":
   format = "%(asctime)s: %(message)s"
   logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S")
   
   logging.info(f'Main: before creating thread')
   x = threading.Thread(target=worker, args=(1, ), daemon=False)
   logging.info(f'Main: before running thread')
   x.start()
   logging.info(f'Main: wait for the thread to finish')
   x.join()
   logging.info(f'Main: all done') 