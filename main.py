import threading
import time

count_threads: int = 50
thread_counter: int = 0;
thread_work: dict[str, int] = {'t1': 0, 't2': 0, 't3': 0}

def thread_body():
    global thread_counter, thread_work
    
    n = threading.current_thread().name
    while thread_counter < 10000:
        thread_counter += 1
        thread_work[n] += 1
        print(f'In thread {n}: thread_counter={thread_counter}')
#        time.sleep(1)


if __name__ == "__main__":
    keys = ['t'+str(x) for x in range(1,count_threads+1)]
    thread_work = {keys[i]:0 for i in range(count_threads)}

    threads = [threading.Thread(target=thread_body, name=x) for x in thread_work.keys()]
    [t.start() for t in threads]
    [t.join() for t in threads]
    
    print(f"Thread couner is {thread_counter}")
    print(f"thread_work is {thread_work}")
    print("Done!")